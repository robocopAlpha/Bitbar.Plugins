#!/bin/bash
# shellcheck disable=SC2128,SC2178,SC2034
#
# <bitbar.title>Open Internet Connections</bitbar.title>
# <bitbar.version>v1.0</bitbar.version>
# <bitbar.author>Deepankar Chakroborty</bitbar.author>
# <bitbar.desc>List lsof output</bitbar.desc>
# <bitbar.image></bitbar.image>
# <bitbar.dependencies>bash</bitbar.dependencies>
# <bitbar.abouturl></bitbar.abouturl>


# Get info

ProgramName=$(lsof -PniTCP +c15| awk '{print $1}' | uniq)
#IPV4andHost=$(lsof -PiTCP +c15| awk '{print $9}' | uniq)


echo "❀"

# Dropdown info
echo "---"

echo "Connected: |color=black $ProgramName";

#echo "---"

#echo "IP and Host: $IPV4andHost";

#echo "---"