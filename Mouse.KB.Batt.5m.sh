#!/bin/bash
# <bitbar.title>Mouse & Keyboard battery</bitbar.title>
# <bitbar.version>0.1</bitbar.version>
# <bitbar.author>Deepankar Chakroborty</bitbar.author>
# <bitbar.author.github></bitbar.author.github>
# <bitbar.desc>Show battery percentage for Bluetooth Mouse & Keyboard</bitbar.desc>
# <bitbar.image></bitbar.image>

MouseBatt=$(ioreg -n BNBMouseDevice | fgrep BatteryPercent | fgrep -v \{ | sed 's/[^[:digit:]]//g')

KBBatt=$(ioreg -n AppleBluetoothHIDKeyboard | fgrep BatteryPercent | fgrep -v \{ | sed 's/[^[:digit:]]//g')

echo "🖱 $MouseBatt %"
echo "⌨ $KBBatt %"
